INSERT INTO locations (street_address, city)
VALUES ('Tverskaya, 15', 'Moscow'),
       ('Yuzhnaya, 10', 'Minsk');

INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
VALUES ('CEO', 'Chief Executive Officer', 1000000, 5000000),
       ('SMM', 'Social media marketing', 250000, 450000),
       ('DEV', 'Developer', 100000, 1000000),
       ('B2B', 'Business-to-Business', 50000, 250000);

INSERT INTO departments (department_id, department_name, manager_id, location_id)
VALUES (1, 'President', 1, 2),
       (2, 'IT development', 2, 3),
       (3, 'Marketing', 3, 1);

INSERT INTO employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id)
VALUES (1, 'Dima', 'Sineguzov', 'dfdfd@gmail.com', '79954542543', '13.04.2020', 'CEO', 2000000, 1, 1),
       (2, 'Masha', 'Petrova', 'dfdsfd@gmail.com', '79955542543', '11.04.2018', 'SMM', 220000, 2, 3),
       (3, 'Roman', 'Dianov', 'dfdfdfdd@gmail.com', '79664542543', '04.04.2016', 'DEV', 334000, 3, 2),
       (4, 'Kirill', 'Dianov', 'dfdffdfdfdd@gmail.com', '79665642543', '04.04.2016', 'DEV', 389000, 4, 2);

INSERT INTO job_history (employee_id, start_date, end_date, job_id, department_id)
VALUES (1, '14.03.2010', '18.04.2020', 'DEV', 2);
CREATE TABLE locations (
                           location_id SERIAL PRIMARY KEY,
                           street_address VARCHAR(40) NOT NULL ,
                           city VARCHAR(30) NOT NULL
);

CREATE TABLE jobs (
                      job_id varchar(10) PRIMARY KEY,
                      job_title varchar(35) NOT NULL,
                      min_salary INTEGER NOT NULL CHECK ( min_salary > 0 ),
                      max_salary INTEGER NOT NULL CHECK ( max_salary > 0 ),
                      CHECK ( max_salary > min_salary )
);

CREATE TABLE departments (
                             department_id INTEGER PRIMARY KEY,
                             department_name varchar(30) NOT NULL,
                             manager_id INTEGER,
                             location_id INTEGER
);

CREATE TABLE employees (
                           employee_id INTEGER PRIMARY KEY,
                           first_name varchar(20),
                           last_name varchar(25),
                           email varchar(25),
                           phone_number varchar(20),
                           hire_date DATE,
                           job_id varchar(10),
                           salary INTEGER check ( salary > 0 ),
                           manager_id INTEGER,
                           department_id INTEGER
);

CREATE TABLE job_history (
                             employee_id INTEGER,
                             start_date DATE,
                             end_date DATE,
                             job_id varchar(10),
                             department_id INTEGER,
                             CONSTRAINT job_history_id PRIMARY KEY (employee_id, start_date),
                             CHECK ( start_date < end_date )
);

ALTER TABLE departments add FOREIGN KEY (location_id) REFERENCES locations (location_id);
ALTER TABLE departments add FOREIGN KEY (manager_id) REFERENCES employees (employee_id);

ALTER TABLE employees add FOREIGN KEY (department_id) REFERENCES departments (department_id);
ALTER TABLE employees add FOREIGN KEY (job_id) REFERENCES jobs (job_id);
ALTER TABLE employees add FOREIGN KEY (manager_id) REFERENCES employees (employee_id);

ALTER TABLE job_history add FOREIGN KEY (employee_id) REFERENCES employees (employee_id);
ALTER TABLE job_history ADD FOREIGN KEY (job_id) REFERENCES jobs (job_id);
ALTER TABLE job_history ADD FOREIGN KEY (department_id) REFERENCES departments (department_id);
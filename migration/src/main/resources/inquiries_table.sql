1)
SELECT last_name, COUNT(*) as amount from employees group by last_name HAVING count(*) > 1;

2)
  SELECT d.department_name, j.max_salary
  FROM departments d
  INNER JOIN employees e
  ON d.department_id = e.department_id
  INNER JOIN jobs j
  ON e.job_id = j.job_id;

3)
SELECT *
FROM employees e
INNER JOIN job_history jh
ON e.employee_id = jh.employee_id;

4)
DELETE FROM locations
WHERE location_id not in
      (
          SELECT min(location_id)
          FROM locations l
          GROUP BY l.street_address, l.city
      );

5)
SELECT * FROM employees LIMIT (SELECT count(*)/2 FROM employees);

6)
SELECT * FROM employees where department_id IS NULL;

7)
SELECT e.first_name, e.last_name, e.salary, d.department_name, l.city
FROM employees e
INNER JOIN departments d on d.department_id = e.department_id
INNER JOIN locations l on d.location_id = l.location_id;

8)

CREATE PROCEDURE show_employees ()
    LANGUAGE sql
    AS $$
SELECT * FROM employees;
$$;

9)

CREATE PROCEDURE up_locations (street_address varchar, city varchar)
    LANGUAGE sql
    AS $$
   INSERT INTO locations (street_address, city)
   VALUES (street_address, city)
$$;

10)
CREATE OR REPLACE FUNCTION snitch() RETURNS event_trigger AS $$
BEGIN
        RAISE NOTICE 'Произошло событие: % %', tg_event, tg_tag;
end;
$$ LANGUAGE plpgsql;

CREATE EVENT TRIGGER snitch ON ddl_command_start EXECUTE PROCEDURE snitch();

11)
SELECT d.department_name, e.salary
FROM departments d
INNER JOIN employees e on d.department_id = e.department_id
WHERE e.salary > 100000 and e.salary < 500000;
